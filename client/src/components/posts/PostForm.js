import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addPost } from '../../actions/post';

const PostForm = ({ addPost }) => {
  const [text, setText] = useState('');
  const [titre , setTitre]=useState('');
  const handleSubmit =(e)=>{
    const post = {
      text, 
      titre
    }
    e.preventDefault();
    addPost({ post });
    setText('');
  }
  return (
    <div className='post-form'>
      <div className='bg-primary p'>
        <h3>Share a Job oppurtunity...</h3>
      </div>
      <form
        className='form my-1'
        onSubmit={handleSubmit}
      >
        <input
        name ='titre'
        value={titre}
        type='text'
        placeholder='Your Job Title'
        onChange={e => setTitre(e.target.value)}

        />
        <textarea
          name='text'
          cols='30'
          rows='5'
          placeholder='Post your Job here'
          value={text}
          onChange={e => setText(e.target.value)}
          required
        />
        <input type='submit' className='btn btn-dark my-1' value='Submit' />
      </form>
    </div>
  );
};

PostForm.propTypes = {
  addPost: PropTypes.func.isRequired
};

export default connect(
  null,
  { addPost }
)(PostForm);
